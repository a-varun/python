def binary_using_ab(n):
    letters = 'AB'
    result = ''
    while n > 0:
        char = letters[n % 2]
        n = n / 2
        result = char + result
    return result

for i in range(1,20):
	print str(i)+' '+binary_using_ab(i)
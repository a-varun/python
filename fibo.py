"""
	For finding the fibonacci series of a number
"""

#Regular
def fibonacci(n):
	j, k = -1, 1
	for i in range(r):
		print j+k,
		j, k= k, j+k

#Returns a list
def give_fibo(n):
	series=[0,1]
	for i in range(n-2):
		series.append(series[-1]+series[-2])
	return series

#Concurrent programming
def get_fiboval(n):
	a,b= -1,1
	for i in range(n):
		yield(a+b)
		a,b=b,a+b


r=int(raw_input("Enter the number of series:"))
#fibonacci(r)
#series=give_fibo(r);
#print series

series=get_fiboval(r)

for i in series:
	print i